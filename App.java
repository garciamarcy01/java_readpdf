package com.basic.java;

import java.io.IOException;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;

public class App {

	public static void main(String[] args) throws java.io.IOException {
		PdfReader reader;
		try {

			reader = new PdfReader("/Users/administrador/Documents/JavaCourse/workspace/JavaBasic/src/com/basic/java/listaM.pdf");
			// pageNumber = 1
			for (int i = 1; i <= 1; i++) {

				String textFromPage = PdfTextExtractor.getTextFromPage(reader, i);

				String s1 = textFromPage.substring(textFromPage.indexOf("Clave") + 0);
				s1.trim();

				String ClaveProducto = removeClaveProducto(s1, "Descripción");
				String Descripción = removeDescription(s1, "Piezas por Caja");
				String Piezas = removePiezas(s1, "Tipo de Medida");
				String Tipo = removeTipo(s1, "Costo Unitario");
				String Costo = removeCosto(s1, "Costo Unitario");
				
				
				System.out.println("==================================================================  Read text from PDF page ==================================================================");
				System.out.println(textFromPage);
				
				System.out.println();
				System.out.println();
				
				System.out.println("==================================================================  Trim text from PDF page ==================================================================");
				System.out.println(s1);
				
				System.out.println();
				System.out.println();
				
				
				System.out.println("==================================================================  Text Formatted to be put in excel separated by ; ==================================================================");
				System.out.print("Pagina " + i + " => ;");
				System.out.print(ClaveProducto + ";");
				System.out.print(Descripción + ";");
				System.out.print(Piezas + ";");
				System.out.print(Tipo + ";");
				System.out.print(Costo);
				System.out.println();
				reader.close();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static String removeDescription(String s1, String s3) {
		String s = s1.substring(0, s1.indexOf(s3));
		String s2 = s.substring(s.indexOf(":") + 2).trim();
		String s4 = s2.substring(s2.indexOf("Descripción") + 12);
		return s4;
	}

	private static String removeClaveProducto(String s1, String s3) {
		String s = s1.substring(0, s1.indexOf(s3));
		String s2 = s.substring(s.indexOf(":") + 2).trim();
		return s2;
	}

	private static String removePiezas(String s1, String s3) {
		String s = s1.substring(0, s1.indexOf(s3));
		String s2 = s.substring(s.indexOf(":") + 2).trim();
		String s4 = s2.substring(s2.indexOf("Piezas por Caja") + 16);

		return s4;
	}

	private static String removeTipo(String s1, String s3) {
		String s = s1.substring(0, s1.indexOf(s3));
		String s2 = s.substring(s.indexOf(":") + 2).trim();

		String s4 = s2.substring(s2.indexOf("Tipo de Medida") + 15);
		return s4;
	}

	private static String removeCosto(String s1, String s3) {
		String s2 = s1.substring(s1.indexOf(s3) + 15).trim();
		return s2;
	}

}


Salidas:
==================================================================  Read text from PDF page ==================================================================
Ultima Modificación por:Xochitl Hernandez      ( 14/01/2019 03:22:03 p. m. )
Catálogo Productos
Elaborado por: Guillermo Vilchis ( 27/06/2008 02:40:22 p. m. )
Clave Producto: 297460
Descripción: CINTA MASK TAPE USO GR.MK-402 P-16
Piezas por Caja: 16
Tipo de Medida: Caja
Costo Unitario: 9


==================================================================  Trim text from PDF page ==================================================================
Clave Producto: 297460
Descripción: CINTA MASK TAPE USO GR.MK-402 P-16
Piezas por Caja: 16
Tipo de Medida: Caja
Costo Unitario: 9


==================================================================  Text Formatted to be put in excel separated by ; ==================================================================
Pagina 1 => ;297460; CINTA MASK TAPE USO GR.MK-402 P-16; 16; Caja;9
